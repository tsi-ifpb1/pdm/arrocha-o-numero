package com.example.arrochaonumero

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import kotlin.random.Random

class MainActivity : AppCompatActivity() {

    private lateinit var tvInferior: TextView
    private lateinit var tvSuperior: TextView
    private lateinit var etChute: EditText
    private lateinit var btChute: Button
    private var NUMERO: Int = 0
    private var INFERIOR: Int = 1
    private var SUPERIOR: Int = 100


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        this.tvInferior = findViewById(R.id.tvInferior)
        this.tvSuperior = findViewById(R.id.tvSuperior)
        this.etChute = findViewById(R.id.etChute)
        this.btChute = findViewById(R.id.btChute)

        this.gerarNumero()

        this.btChute.setOnClickListener({ this.chutar() })
    }


    override fun onResume() {
        super.onResume()

        this.gerarNumero()
    }

    private fun gerarNumero() {
        this.NUMERO = Random.nextInt(2,100)

        Log.i("APP_ARROCHA", this.NUMERO.toString())

        this.INFERIOR = 1
        this.SUPERIOR = 100

        this.tvInferior.text = "1"
        this.tvSuperior.text = "100"
    }

    private fun chutar() {
        if (this.etChute.text.toString() == "") {
            Toast.makeText(this, "Digite um número!", Toast.LENGTH_SHORT).show()
        } else {
            val chute = this.etChute.text.toString().toInt()
            if ((chute == this.NUMERO) || (chute <= this.INFERIOR) || (chute >= this.SUPERIOR)) {
                this.perdeu()
            } else if (chute < NUMERO) {
                this.INFERIOR = chute
                this.tvInferior.text = chute.toString()
            } else {
                this.SUPERIOR = chute
                this.tvSuperior.text = chute.toString()
            }
            if ((this.SUPERIOR - this.INFERIOR) == 2) {
                this.ganhou()
            }
            this.etChute.text.clear()
        }
    }

    private fun perdeu() {
        val it = Intent(this@MainActivity, ResultadoActivity::class.java)
        it.putExtra("RESULTADO", "gameover")
        startActivity(it)
    }

    private fun ganhou() {
        val it = Intent(this@MainActivity, ResultadoActivity::class.java)
        it.putExtra("RESULTADO", "win")
        startActivity(it)
    }
}
