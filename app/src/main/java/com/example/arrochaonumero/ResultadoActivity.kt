package com.example.arrochaonumero

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class ResultadoActivity : AppCompatActivity() {

    private lateinit var tvResultado: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_resultado)

        this.tvResultado = findViewById(R.id.tvResultado)

        val msg = intent.getStringExtra("RESULTADO")

        if (msg == "gameover") {
            this.tvResultado.text = "Você perdeu"
            this.tvResultado.setBackgroundColor(Color.RED)
            this.tvResultado.setTextColor(Color.WHITE)
        } else {
            this.tvResultado.text = "Você ganhou"
            this.tvResultado.setBackgroundColor(Color.GREEN)
        }


    }
}
